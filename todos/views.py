from django.shortcuts import render,get_object_or_404,redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm#, TodoItemForm

# Create your views here.

def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list":todos
    }
    return render(request,"todos/todos.html",context)

def todo_list_detail(request,id):
    todo_list_detail = get_object_or_404(TodoList,id=id)
    context = {
        "todo_list_detail_object":todo_list_detail
    }
    return render(request,"todos/details.html",context)

def todo_list_create(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect('todo_list_detail', list.id)
    else:
        form = TodoListForm()
        context = {
            'form': form,
        }
    return render(request, 'todos/create.html', context)


def todo_list_update():
    return None

def todo_list_delete():
    return None

#def todo_item_create(request):
#    if request.method == 'POST':
#        form = TodoItemForm(request.POST)
#        if form.is_valid():
#            list = form.save()
#            return redirect('todo_list_detail', list.id)
#    else:
#        form = TodoItemForm()
#        context = {
#            'form': form,
#        }
#    return render(request, 'todos/create.html', context)

def todo_item_update():
    return None